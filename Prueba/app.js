window.onload = function () {
    init();
}

var canvas, context;

function init() {
    //Capturar contenedor
    var contendedor = document.getElementById("lienzo");
    //Rellenar canvas
    canvas = document.createElement("canvas");
    canvas.width = 200;
    canvas.height= 200;
    contendedor.appendChild(canvas);

    //Almacenar contexto
    context = canvas.getContext("2d");

    dibujarTexto();
}


function dibujarTexto(){
    //Elegir color
    context.fillStyle = "#FFF";
    //Elegir tipo de letra
    context.font = "20px _sans";
    //
    context.fillText("Hola mundo", 10, 20);
}